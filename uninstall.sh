#!/bin/bash

# Options
SHOW_HELP=false
VERBOSE=false

# Configuration options
VIM_TARGET=~/.vim

while getopts ":hv" opt; do
    case ${opt} in
        h )
            SHOW_HELP=true
            ;;
        v )
            VERBOSE=true
            ;;
        \? )
            echo "Invalid option -${OPTARG}"
            exit 1
            ;;
    esac
done

if $VERBOSE; then
    exec 3>&1
else
    exec 3>/dev/null
fi

if [[ -h $VIM_TARGET ]]; then
    echo "INFO: Removing $VIM_TARGET" >&3
    rm -rf $VIM_TARGET
fi

