" Display & Format

  " Enable line numbering
  set number

  " Turn on syntax highlighting
  syntax on

  " Number of spaces for display a tab
  set tabstop=4

  " Column highlighting
  set colorcolumn=80
  highlight ColorColumn ctermbg=darkgrey

  " Tabs width (displayed)
  set shiftwidth=4

  " Theme
  syntax enable
  colorscheme monokai

  " Show whitespaces
  set list
  nmap <leader>l :set list!<CR>
  set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,space:·
  highlight NonText ctermbg=bg
  highlight SpecialKey ctermbg=bg
    " Other useful characters

""" Settings

  "Enable autoread
  set autoread

  "Disable swap files
  set noswapfile

  " Yank to system clipboard (only with clipboard support)
  set clipboard=unnamedplus

  " Yank to global temporary file
  vmap <leader>y :w! /tmp/vitmp<CR>
  nmap <leader>p :r! cat /tmp/vitmp<CR>

  " Spell checking
  nmap <silent> <leader>s :set spell!<CR>
  " Set region to British English
  set spelllang=en_gb

  " Autoremove trailing spaces on save
  autocmd BufWritePre * :%s/\s\+$//e

  "Comments
  autocmd FileType python setlocal commentstring=#\ %s
  autocmd FileType cpp setlocal commentstring=//\ %s
  autocmd FileType c setlocal commentstring=//\ %s

""" Useful Extensions

  "" Ctags

  set tags=tags;/
  nmap <F8> :TagbarToggle<CR>

  "" Airline

  set laststatus=2
  let g:airline_theme='molokai'
