#!/bin/bash

# Options
SHOW_HELP=false
FORCE=false
VERBOSE=false

# Configuration variables
VIM_SOURCE=`pwd`
VIM_TARGET=~/.vim

while getopts ":hvf" opt; do
    case ${opt} in
        h )
            SHOW_HELP=true
            ;;
        f )
            FORCE=true
            ;;
        v )
            VERBOSE=true
            ;;
        \? )
            echo "Invald option -${OPTARG}"
            exit 1
            ;;
    esac
done

if $VERBOSE; then
    exec 3>&1
else
    exec 3>/dev/null
fi

if ! $FORCE; then
    if [[ -d $VIM_TARGET ]] && [[ ! -h $VIM_TARGET ]]; then
        echo "INFO: Directory ${VIM_TARGET} already exists! Moving to ${VIM_TARGET}.bak..."
        mv ${VIM_TARGET}/ ${VIM_TARGET}.bak/
    fi

    if [[ -h $VIM_TARGET ]]; then
        LINK_TARGET=`readlink $VIM_TARGET`
        echo "INFO: Symbolic link to ${LINK_TARGET} exists! Moving to ${VIM_TARGET}.bak..."
        mv ${VIM_TARGET} ${VIM_TARGET}.bak
    fi

    echo "INFO: Creating link to ${VIM_TARGET}" >&3
    ln -s ${VIM_SOURCE} ${VIM_TARGET}
else

    echo "INFO: (FORCE) Creating link to ${VIM_TARGET}" >&3
    ln -f -s ${VIM_SOURCE} ${VIM_TARGET}
fi

